#!/bin/sh
_term() {
  echo "Caught SIGTERM signal!"
}

_termint() {
  echo "Caught SIGINT signal!"
}

trap _term SIGTERM
trap _termint SIGINT


curl --location --request POST 'http://kong.planblick.svc:8001/services/' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "easy2track-website",
  "url": "http://easy2track-website.planblick.svc:80"
}'


curl --location --request POST 'http://kong.planblick.svc:8001/services/easy2track-website/routes/' \
--header 'Content-Type: application/json' \
--data-raw '{
  "hosts": ["easy2track.de", "www.easy2track.de", "staging.easy2track.de"]
}'

nginx